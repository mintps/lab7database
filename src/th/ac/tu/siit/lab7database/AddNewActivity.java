package th.ac.tu.siit.lab7database;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

public class AddNewActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_new);
		
		Intent i = this.getIntent();
		if(i.hasExtra("id")){ //Check if there are extras with the intent
			//Load the value and set the to the widgets
			
			((EditText)findViewById(R.id.etName)).setText(i.getStringExtra("ct_name"));
			((EditText)findViewById(R.id.etPhone)).setText(i.getStringExtra("ct_phone"));
			((EditText)findViewById(R.id.etEmail)).setText(i.getStringExtra("ct_email"));
			RadioGroup rdg = (RadioGroup)findViewById(R.id.rdgType);
			int ptype = i.getIntExtra("ct_type", 0);
			if(ptype == R.drawable.home)
				rdg.check(R.id.rdHome);
			else
				rdg.check(R.id.rdMobile);
			
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.add_new, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case R.id.action_save:
			EditText etName = (EditText)findViewById(R.id.etName);
			EditText etPhone = (EditText)findViewById(R.id.etPhone);
			EditText etEmail = (EditText)findViewById(R.id.etEmail);
			RadioGroup rdg = (RadioGroup)findViewById(R.id.rdgType);
			
			String sName = etName.getText().toString().trim();
			String sPhone = etPhone.getText().toString().trim();
			String sEmail = etEmail.getText().toString().trim();
			int iType = rdg.getCheckedRadioButtonId();
			
			if (sName.length() == 0 || sPhone.length() == 0 || iType == -1) {
				Toast t = Toast.makeText(this, 
						"Contact name,  phone, and type are required", 
						Toast.LENGTH_LONG);
				t.show();
			}
			else {
				Intent i = this.getIntent();
				Intent data = new Intent();
				data.putExtra("name", sName);
				data.putExtra("phone", sPhone);
				data.putExtra("email", sEmail);
				data.putExtra("id", i.getLongExtra("id", -1));
				
				switch(iType) {
				case R.id.rdHome:
					data.putExtra("type", String.valueOf(R.drawable.home));
					break;
				case R.id.rdMobile:
					data.putExtra("type", String.valueOf(R.drawable.mobile));
					break;
				}
				setResult(RESULT_OK, data);
				finish();
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	
}
