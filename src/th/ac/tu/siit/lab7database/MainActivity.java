package th.ac.tu.siit.lab7database;

import android.os.Bundle;
import android.app.ListActivity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends ListActivity {
	
	DBHelper dbHelper;
	SQLiteDatabase db;
	Cursor cursor; //for managing the records retrieved from the table, it works like List<Map<String,String>> in the previous lab.
	SimpleCursorAdapter adapter; //use cursor as the data source

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		dbHelper = new DBHelper(this);
		db = dbHelper.getWritableDatabase(); //we can update the database.
		cursor = getAllContacts();
		adapter = new SimpleCursorAdapter(this, R.layout.item, cursor, 
				new String[] {"ct_name", "ct_phone", "ct_type", "ct_email"},
				new int[] {R.id.tvName, R.id.tvPhone, R.id.ivPhoneType, R.id.tvEmail}, 0);
		setListAdapter(adapter);
		registerForContextMenu(getListView());
	}
	
	private Cursor getAllContacts() {
		//db.quary executes "SELECT" statement
		return db.query("contacts", // table name
				new String[] {}, //list of columns
				null, // conditions for WHERE clause e.g. "ct_name LIKE ?"
				null, // value for the condition e.g. new String[]{"John%"}
				null, // GROUP BY
				null, // HAVING
				"ct_name asc");// ORDER BY
		//SELECT _id, ct_name, ct_phone, ct_type, ct_email FROM contacts ORDER bY ct_name ASC;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		getMenuInflater().inflate(R.menu.context, menu);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		// Closing everything before letting the application ends
		cursor.close();
		db.close();
		dbHelper.close();
		finish();
	}

	@Override
	protected void onActivityResult(int requestCode, 
			int resultCode, Intent data) {
		if (requestCode == 9999 && resultCode == RESULT_OK) {
			// Insert the new record into the table
			// Content view is a class for storing key-value pairs for the tables
			ContentValues v = new ContentValues();
			v.put("ct_name", data.getStringExtra("name"));
			v.put("ct_phone", data.getStringExtra("phone"));
			v.put("ct_email", data.getStringExtra("email"));
			v.put("ct_type", data.getStringExtra("type"));
			db.insert("contacts", null, v);
			// Refresh the ListView the display the record
			// 1. Re-retrieve all the record 
			cursor = getAllContacts();
			// 2. Update the cursor in the adapter
			adapter.changeCursor(cursor);
			// 3. Notify that the data have changed
			adapter.notifyDataSetChanged();
		}
		else if (requestCode == 8888 && resultCode == RESULT_OK){
			ContentValues v = new ContentValues();
			long id = data.getLongExtra("id", -1);
			v.put("ct_name", data.getStringExtra("name"));
			v.put("ct_phone", data.getStringExtra("phone"));
			v.put("ct_email", data.getStringExtra("email"));
			v.put("ct_type", data.getStringExtra("type"));
			String selection = "_id= ?";
			String[] selectionArgs = {String.valueOf(id)}; 
			db.update("contacts", v, selection, selectionArgs);
			// 1. Re-retrieve all the record 
			cursor = getAllContacts();
			// 2. Update the cursor in the adapter
			adapter.changeCursor(cursor);
			// 3. Notify that the data have changed
			adapter.notifyDataSetChanged();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case R.id.action_new:
			Intent i = new Intent(this, AddNewActivity.class);
			startActivityForResult(i, 9999);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo a = (AdapterContextMenuInfo)item.getMenuInfo();
		long id = a.id; // id of the selected item
		int position = a.position; // position of the selected item
		
		switch(item.getItemId()) {
		case R.id.action_edit:
			Cursor c = (Cursor)adapter.getItem(position); // get the selected record
			String name = c.getString(c.getColumnIndex("ct_name")); // get the value of "ct_name" column
			String phone = c.getString(c.getColumnIndex("ct_phone"));
			String email = c.getString(c.getColumnIndex("ct_email"));
			int itype = c.getInt(c.getColumnIndex("ct_type"));
			
			Intent i = new Intent(this,AddNewActivity.class);
			// Attach the data to the Intent
			i.putExtra("ct_name", name);
			i.putExtra("ct_phone", phone);
			i.putExtra("ct_email", email);
			i.putExtra("ct_type", itype); //long -> getIntExtra
			i.putExtra("id", id); // long -> getLongExtra
			
			startActivityForResult(i, 8888); //start AddNewActivity and wait for the result
			
			Toast t = Toast.makeText(this, "Selected ID = "+id+
					" with name = "+name, Toast.LENGTH_LONG);
			t.show();
			return true;
		case R.id.action_delete:
			String selection = "_id= ?";
			String[] selectionArgs = { String.valueOf(id) }; 
			db.delete("contacts", selection, selectionArgs);
			// 1. Re-retrieve all the record 
			cursor = getAllContacts();
			// 2. Update the cursor in the adapter
			adapter.changeCursor(cursor);
			// 3. Notify that the data have changed
			adapter.notifyDataSetChanged();
			return true;
		}
		return super.onContextItemSelected(item);
	}
}
