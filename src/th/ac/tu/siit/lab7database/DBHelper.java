package th.ac.tu.siit.lab7database;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
	
	private static final String DBNAME = "contacts.db"; //save in this database
	private static final int DBVERSION = 1;
	
	public DBHelper(Context ctx) {
		super(ctx, DBNAME, null, DBVERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {	
		//called when the application is run, and there is no database file in the internal storage
		String sql = "CREATE TABLE contacts (" +
				"_id integer primary key autoincrement, " +
				"ct_name text not null, " +
				"ct_phone text not null, " +
				"ct_type integer default 0, " +
				"ct_email text not null);";
		db.execSQL(sql);
		//the primary key of the table need to be "_id" when we want to display the data in list view
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// when the database file exists, but the DBVERSION is higher that the version in the file
		String sql = "DROP TABLE IF EXISTS contacts;";
		db.execSQL(sql);
		this.onCreate(db);
		//This is not recommended in practical because all records in the table will be removed. we should "ALTER TABLE" instead.
	}

}
